﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netsen_GroupTest.Model
{
    class Auteur
    {
        public int AuteurId { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }

        public Auteur(string nom, string prenom, string adresse)
        {
            this.Nom = nom;
            this.Prenom = prenom;
            this.Adresse = adresse;
        }

        public List<Post> Posts { get; } = new List<Post>();
        public List<Comment> Comments { get; } = new List<Comment>();




    }
}
