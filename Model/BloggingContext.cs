﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Netsen_GroupTest.Model
{
    class BloggingContext : DbContext
    {


        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Auteur> Auteurs { get; set; }
        public DbSet<Comment> Comments { get; set; }

        //Must put on Data Source the URL of your created sqlite DB
        protected override void OnConfiguring(DbContextOptionsBuilder options)
                        => options.UseSqlite("Data Source =C:\\Users\\isebb\\Documents\\Portfolio\\NetsenGroupExercice\\maDB.db");

       







    }
}
