﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Netsen_GroupTest.Model
{
    class Comment
    {
        public int CommentId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
     


        public Comment(string Title, string content)
        {
            this.Title = Title;
            this.Content = content;
        }
        public Comment(string title, string Content, int auteurId, int PostId)
        {
            this.Title = title;
            this.Content = Content;
            this.AuteurId = auteurId;
            this.PostId = PostId;

        }

       


        public int PostId { get; set; }
        public Post Post { get; set; }

        public int? AuteurId { get; set; }
        public Auteur Auteur { get; set; }
    }
}
