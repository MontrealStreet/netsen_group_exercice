﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netsen_GroupTest.Model
{
    class Blog
    {

        public int BlogId { get; set; }
        public string Url { get; set; }

        public Blog(string Url)
        {
            this.Url = Url;
        }


        public Post addPost(string titre,string content,int auteurId)
        {
            return new Post(titre, content, auteurId, this.BlogId);
        }
        public List<Post> Posts { get; } = new List<Post>();
    }
}
