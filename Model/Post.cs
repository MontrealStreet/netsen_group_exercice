﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netsen_GroupTest.Model
{
    class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
     


        public int BlogId { get; set; }
        public Blog Blog { get; set; }
        public int AuteurId { get; set; }
        public Auteur Auteur { get; set; }

        public Post(string title, string Content,  int auteurId, int BlogId)
        {
            this.Title = title;
            this.Content = Content;
            this.AuteurId = auteurId;
            this.BlogId = BlogId;
           
        }

        public Comment addComment(string titre, string content, int auteurId)
        {
            return new Comment(titre, content, auteurId, this.PostId);
        }
      

        public List<Comment> Comments { get; } = new List<Comment>();
    }
}
