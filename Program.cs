﻿using Netsen_GroupTest.Model;
using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace NetsenGroupExercice
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new BloggingContext())
            {

                try
                {
                    // Create
                    Console.WriteLine("Inserting a new blog");
                    //db.Add(new Blog("www.Blog_A.com"));
                    //db.Add(new Blog("www.Blog_B.com"));
                    //db.Add(new Auteur("ilyas", "sebbar", "101"));
                    //db.Add(new Auteur("Robert", "Deniro", "105"));
                    //db.SaveChanges();

                    ////Read
                    Console.WriteLine("Querying for a blog");

                    var blog = db.Blogs
                        .OrderBy(b => b.BlogId)
                        .FirstOrDefault();

                    var blogs = from b in db.Blogs
                                orderby b.BlogId, b.Url
                                select b;
                    foreach (var b in blogs)
                    {
                        Console.WriteLine("The id of the  blog is {0}  and the Url is {1}", b.BlogId, b.Url);

                    }

                    var auteur = from a in db.Auteurs
                                 orderby a.AuteurId, a.Nom, a.Prenom, a.Adresse
                                 select a;
                    foreach (var a in auteur)
                    {
                        Console.WriteLine("The id of the autor is {0}  and complete Name is :{1} {2} -- Adresse :{3}", a.AuteurId, a.Prenom, a.Nom, a.Adresse);

                    }

                    //Update

                    // getting data from db
                    Post post1 = db.Posts.OrderBy(p => p.PostId).FirstOrDefault();
                    Post post2 = db.Posts.OrderBy(p => p.PostId).Skip(1).FirstOrDefault();

                    Auteur auteur1 = db.Auteurs.OrderBy(a => a.AuteurId).FirstOrDefault();
                    Auteur auteur2 = db.Auteurs.OrderBy(a => a.AuteurId).Skip(1).FirstOrDefault();


                    Comment comment1 = db.Comments.OrderBy(c => c.CommentId).FirstOrDefault();
                    Comment comment2 = db.Comments.OrderBy(c => c.CommentId).Skip(1).FirstOrDefault();

                    //Adding posts
                    Console.WriteLine("Updating the blog and adding a post");
                    

                    blog.Url = "www.Blog_A.com";
                    Console.WriteLine("We Are in the blog  {0} :" ,blog.Url);
                    db.Add(blog.addPost("myTitle", "myContent", auteur1.AuteurId));
                    db.Add(blog.addPost("myTitle2", "myContent2", auteur2.AuteurId));
                    db.SaveChanges();

                    //Adding Comments
                    Console.WriteLine("Updating the blog and adding a Comment on a Post");
                    Console.WriteLine("We Are in the blog  {0} :", blog.Url);
                    //db.Add(post1.addComment("myCommentTitle", "MYCommmentContent", auteur1.AuteurId));
                    //db.Add(post2.addComment("myCommentTitle2", "MYCommmentContent2", auteur2.AuteurId));
                    db.SaveChanges();

                    // Delete
                    Console.WriteLine("Delete the blog");
                    //db.Blogs.Remove(blog);

                    // delete permmited only for the author of comment or post

                    //if (comment2.AuteurId == auteur2.AuteurId )
                    //{
                    //    db.Comments.Remove(comment2);
                    //}
                    //else if(comment1.AuteurId == auteur1.AuteurId)
                    //{
                    //    db.Comments.Remove(comment1);
                    //}
                    //else
                    //{
                    //    Console.WriteLine("Action not Permited");
                    //}

                    db.SaveChanges();


                    Console.WriteLine("is Done");
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught .", e);
                }
            }
        }
        
    }

}
